<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.gs4tr.punchcard</groupId>
        <artifactId>gs4tr-punchcard-application-pom</artifactId>
        <version>6.0.8-SNAPSHOT</version>
        <relativePath>../gs4tr-punchcard-application-pom</relativePath>
    </parent>

    <artifactId>gs4tr-punchcard-docker-application-pom</artifactId>
    <packaging>pom</packaging>
    <name>Project Punchcard Docker Application Pom</name>

    <dependencies>
        <dependency>
            <groupId>org.gs4tr.punchcard</groupId>
            <artifactId>gs4tr-punchcard-docker</artifactId>
            <type>zip</type>
        </dependency>
    </dependencies>

    <profiles>
        <profile>
            <id>application</id>
            <activation>
                <file>
                    <exists>src/main/java</exists>
                </file>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-enforcer-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>enforce-property</id>
                                <goals>
                                    <goal>enforce</goal>
                                </goals>
                                <configuration>
                                    <rules>
                                        <requireProperty>
                                            <property>start.class</property>
                                            <message>You must set a start.class property!</message>
                                        </requireProperty>
                                    </rules>
                                    <fail>true</fail>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-dependency-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>third-party</id>
                                <phase>validate</phase>
                                <goals>
                                    <goal>unpack-dependencies</goal>
                                </goals>
                                <configuration>
                                    <includeArtifactIds>gs4tr-punchcard-docker</includeArtifactIds>
                                    <outputDirectory>${project.build.directory}/tmp</outputDirectory>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-resources-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>third-party</id>
                                <phase>compile</phase>
                                <goals>
                                    <goal>copy-resources</goal>
                                </goals>
                                <configuration>
                                    <outputDirectory>${project.build.directory}/docker</outputDirectory>
                                    <resources>
                                        <resource>
                                            <directory>${project.build.directory}/tmp/docker</directory>
                                            <filtering>true</filtering>
                                            <includes>
                                                <include>**/docker-compose.yml</include>
                                                <include>**/prometheus.yml</include>
                                                <include>**/entrypoint.sh</include>
                                                <include>**/oidc/config/config.json</include>
                                            </includes>
                                        </resource>
                                        <resource>
                                            <directory>${project.build.directory}/tmp/docker</directory>
                                            <excludes>
                                                <exclude>**/docker-compose.yml</exclude>
                                                <exclude>**/prometheus.yml</exclude>
                                                <exclude>**/entrypoint.sh</exclude>
                                                <include>**/oidc/config/config.json</include>
                                            </excludes>
                                        </resource>
                                    </resources>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>com.google.cloud.tools</groupId>
                        <artifactId>jib-maven-plugin</artifactId>
                        <configuration>
                            <from>
                                <image>eclipse-temurin:21-jre</image>
                            </from>
                            <to>
                                <image>${project.artifactId}:latest</image>
                            </to>
                            <extraDirectories>
                                <paths>${project.build.directory}/docker/jib</paths>
                            </extraDirectories>
                            <outputPaths>
                                <tar>target/jib-image-${project.version}.tar</tar>
                            </outputPaths>
                            <container>
                                <entrypoint>
                                    <shell>sh</shell>
                                    <option>-c</option>
                                    <arg>chmod +x /entrypoint.sh &amp;&amp; sync &amp;&amp;
                                        /entrypoint.sh
                                    </arg>
                                </entrypoint>
                                <creationTime>USE_CURRENT_TIMESTAMP</creationTime>
                            </container>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

</project>